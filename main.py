from langchain.utilities import SQLDatabase
from langchain.llms import OpenAI
from langchain_experimental.sql import SQLDatabaseChain
import os
from dotenv import load_dotenv

load_dotenv()
key = os.getenv('OPENAI_API_KEY')

db_password = os.getenv('DB_PASSWORD')
db_host = os.getenv('DB_HOST')
db_user = os.getenv('DB_USER') 
db_name = os.getenv('DB_NAME') 

db = SQLDatabase.from_uri(f"mysql+pymysql://{db_user}:{db_password}@{db_host}/{db_name}")

llm = OpenAI(temperature=0, verbose=True, openai_api_key=key)
db_chain = SQLDatabaseChain.from_llm(llm, db, verbose=True)
db_chain.run("How many emotions I have recognized in the last year?")